<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use WithFaker;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection|Article
     */
    private $article;

    /**
     * @var Collection|Article[]
     */
    private $articles;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = \App\Models\User::factory()->create();
        $this->articles = \App\Models\Article::factory()->count(10)->for($this->user)->create();
        $this->article = $this->articles->first();
    }

    /**
     * Success get all article.
     * @group articles
     * @return void
     */
    public function test_can_get_all_articles()
    {
        $request =$this->getJson(route('articles.index'));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->articles->count(), $payload->data);
    }

    /**
     * Failed get article.
     * @group articles
     * @return void
     */
    public function test_get_article()
    {
        $article = $this->articles->random();
        $request =$this->getJson(route('articles.show', ['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Success get article.
     * @group articles
     * @return void
     */
    public function test_success_get_article()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.show', ['article' => $article])]
        );
        $request =$this->getJson(route('articles.show', ['article' => $article]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($article->id, $payload->data->id);
        $this->assertArrayHasKey('title', (array)$payload->data);
        $this->assertEquals($article->title, $payload->data->title);
        $this->assertArrayHasKey('content', (array)$payload->data);
        $this->assertEquals($article->content, $payload->data->content);
    }

    /**
     * Success create article.
     * @group articles
     * @return void
     */
    public function test_can_create_article()
    {
        $user = \App\Models\User::factory()->create();
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $data = [
            'title' => $this->faker->sentence(),
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id
        ];
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('articles', $data);
        $this->assertDatabaseHas('articles', ['id' => $payload->data->id]);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_title_validation_error()
    {

        $data = [
            'content' => $this->faker->paragraph(6),
            'user_id' => $this->user->id,
        ];
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * Failed create article.
     * @group articles
     * @return void
     */
    public function test_validation_error()
    {
        $data = [];
        Passport::actingAs($this->user,
            [route('articles.store')]
        );
        $request = $this->postJson(route('articles.store'), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
        $this->assertArrayHasKey('content', (array)$payload->errors);
        $this->assertArrayHasKey('user_id', (array)$payload->errors);
    }


    /**
     * Success update article.
     * @group articles
     * @return void
     */
    public function test_success_update_article()
    {
        $user = \App\Models\User::factory()->create();
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.update',['article' => $article])]
        );
        $response = $this->json('put',route('articles.update', ['article' => $article]));
        $response->assertStatus(200);
    }


    /**
     * Failed update article.
     * @group articles
     * @return void
     */
    public function test_content_validation_error()
    {
        $article = $this->articles->random();
        $data = [
            'title' => $this->faker->paragraph(2),
            'user_id' => $this->user->id,
        ];
        Passport::actingAs(
            $this->user,
            [route('articles.update', ['article' => $article])]
        );
        $request = $this->postJson(route('articles.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('content', (array)$payload->errors);
    }


    /**
     * Success destroy article.
     * @group articles
     * @return void
     */
    public function test_success_destroy_article()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.destroy',['article' => $article])]
        );
        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertNoContent();
    }


    /**
     * Failed destroy article.
     * @group articles
     * @return void
     */
    public function test_failed_destroy_article()
    {
        $article = $this->articles->random();
        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertUnauthorized();;
    }

}
