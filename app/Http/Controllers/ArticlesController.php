<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ArticlesController extends Controller
{
    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }


    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $articles = Article::all();
        return ArticleResource::collection($articles);
    }



    public function store(ArticleRequest $request)
    {
        $article = Article::create($request->validated());
        return new ArticleResource($article);
    }


    /**
     * @param Article $article
     * @return ArticleResource
     */
    public function show(Article $article): ArticleResource
    {
        return new ArticleResource($article);
    }


    /**
     * @param Request $request
     * @param Article $article
     * @return ArticleResource
     */
    public function update(Request $request, Article $article): ArticleResource
    {
        $article->update($request->all());
        return new ArticleResource($article);
    }


    /**
     * @param Article $article
     * @return Response
     */
    public function destroy(Article $article): Response
    {
        $article->delete();
        return response('', 204);
    }
}
