<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('articles', App\Http\Controllers\ArticlesController::class);

Route::apiResource('comments', App\Http\Controllers\CommentsController::class);

Route::post('articles/{article}/comments', [App\Http\Controllers\CommentsController::class, 'store'])
    ->name('comments.store');
